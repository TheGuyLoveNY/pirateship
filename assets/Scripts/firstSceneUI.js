cc.Class({
    extends: cc.Component,

    properties: 
	{},

    onLoad: function () 
	{

		//Resizes with the aspect ratio
		cc.view.resizeWithBrowserSize(true); 

		//If the button is pressed, execute buttonClicked() function
		this.node.on('click', this.buttonClicked, this); 
    },

    buttonClicked: function (event)  
	{
			//Load the First scene.
			cc.director.loadScene("MainScene");  
    }

});
