cc.Class({
    extends: cc.Component,

    properties: {},

    // use this for initialization
    onLoad: function () 
	{
		var theParent = cc.director.getScene(); 
		var goingUp = false;
		
    },

    // called every frame
    update: function (dt) 
	{ 
		this.Action();
    }, 

	Action : function()
	{
		//Roll the crates down, once reached max limit, destroy them.
		this.node.y -= 2;   
		if(this.node.y < 0)
			this.node.destroy();  
			
	},
});
