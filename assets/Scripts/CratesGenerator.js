cc.Class({
    extends: cc.Component,


    properties: 
	{

		//Left Crate  
		prefab:
		{
			default : null,
			type : cc.Prefab
		},

		//Right Crate  
		prefab2:
		{
			default : null,
			type : cc.Prefab
		},

		//current timer when the game starts
		curTimer : 0.0,   

		//number to spawn left and right crate consequently
		num : 0,


		//When to spawn the crates (In secs)
		spawnTime : 1,				

    }, 

    // use this for initialization
    onLoad: function () 
	{
	  
		var spawned = false;
		var curObj = null;

    },

	spawnRight : function()
	{

		//create a right crate and reset it's position.
		this.curObj = cc.instantiate(this.prefab);
		this.curObj.parent = cc.director.getScene();
		this.curObj.setPosition(300,750);    
		this.spawned = false;
		this.curTimer = 0; 
	},

	spawnLeft: function()
	{

		//create a right crate and reset it's position.
		this.curObj = cc.instantiate(this.prefab2);
		this.curObj.parent = cc.director.getScene(); 
		this.curObj.setPosition(100,750);    
		this.spawned = false;
		this.curTimer = 0; 
	},

    // Called every frame of the game
    update: function (dt) 
	{
	 
	
		 if(this.curTimer > this.spawnTime && (!this.spawned))				//It is time to spawn? and can we spawn already?
		 {
			this.num += 1;			
			this.spawned = true; 

			if((this.num % 2) == 0)											//If even, spawn right crate, or left crate otherwise.
				this.spawnRight();

			else
				this.spawnLeft(); 
		 }


		 this.curTimer +=  (dt * 2);										//the timer.


		//console.log(parseFloat(this.curTimer));							//For debugging

    },
});
