cc.Class({
    extends: cc.Component,

    properties: 
	{
		theSpeed: 0,				//the speed at which the background will move
		maxLimit: 0,				//The last limit at which the bg will be resetted.
		restY : 0,					//Reset position.


		//background sprite
		prefab:
		{
			default : null,
			type : cc.Prefab    
		} 
    },

    // use this for initialization
    onLoad: function () 
	{

		var theParent = cc.director.getScene(); 
		var goingUp = false;
		
    },

    // called every frame
    update: function (dt) 
	{ 
		//roll the background.
		this.node.y -= 2 * dt * this.theSpeed;     
		 

		 //Reset once passed the reset limit.
		if(this.node.y < this.maxLimit) 
			this.node.setPosition(0,this.restY);    
    }, 
});
