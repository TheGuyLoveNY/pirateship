cc.Class({
    extends: cc.Component,

    properties: 
	{
		bg : 
		{
			default: null,
			type : cc.Sprite
			
		},		
		
		scoreText:
		{
			default : null,
			type: cc.Label
		},

		leap: 200,				//The distance it will turn

		boat :
		{
			default: null,
			type: cc.Sprite 
		},

		score : 0,
		alive : true,
		blast:
		{
			default : null,
			type: cc.Animation 
		},

		blastSound:
		{
			default: null, 
			type: cc.AudioSource
		}

    },

	
    // use this for initialization
    onLoad: function () 
	{

		var moved = false;
		var theBack = this.bg;  
		var myBoat = this.boat;   

		this.alive = true;
		//Resizes with the aspect ratio
		cc.view.resizeWithBrowserSize(true); 






		//Handle an event when user clicks mouse or presses space bar
		this.node.on('mousedown',this.mouseDown,this); 
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN,this.buttonDown,this); 

		var manager = cc.director.getCollisionManager(); 
		manager.enabled = true;


			//Pre-load the scene
		cc.director.preloadScene("MainScene", function () {
	
});
   
    },   
	 
	buttonDown : function(event)  
	{
		if(event.keyCode == cc.KEY.space)   
			this.moved = true;
	}, 

	mouseDown : function(event)  
	{	
		this.moved = true		//aint working
	}, 

	MoveLeft : function()
	{

		var moveAction = cc.moveBy(0.2, -this.leap,0);   
		this.node.runAction(moveAction); 

		this.moved = false; 
	}, 

	MoveRight : function() 
	{
		//thespeed, x, y
		var moveAction = cc.moveBy(0.2, this.leap,0);       
		this.node.runAction(moveAction);  
		this.moved = false; 
	},

    // called every frame, uncomment this function to activate update callback
    update: function (dt) 
	{
		//this.schedule(function fe(){ this.myBoat.node.position.y += 1 },1); 
		if(this.moved)
		{
		  
			if(this.node.x > 50)   
				this.MoveLeft();  

			else 
				this.MoveRight();  
		}


		if(this.node.x > 180 || this.node.x < -180)
			this.node.x = 120; 


		if(this.alive)
		{
			this.score += dt;  
			this.scoreText.string = ""+Math.floor(this.score);
		}
		else
		{
			this.scoreText.string = "Max Score : "+ Math.floor(this.score);
		}

    },   

	Restart : function()
	{
		cc.director.loadScene("MainScene"); 
	},

	onCollisionEnter: function(other, self)
	{ 
		console.log(Math.floor(this.score)); 
		this.alive = false;
		this.blast.play();  
		this.blastSound.play();
		this.schedule(this.Restart,3);    
	
		//add boat blast effect, and restart afte few seconds.
	},
});  
