cc.Class({
    extends: cc.Component,


	//all the properties 
    properties: { 


		//Defined the variable here, of type label
        label: {
            default: null,
            type: cc.Label
        },
        // defaults, set visually when attaching this script to the Canvas
        text:'Hello, World!',
    },

    // use this for initialization
    onLoad: function () {
        this.label.string = this.text;
		var myBut = this.getComponent(cc.Button);  

		//Resizes with the aspect ratio
		cc.view.resizeWithBrowserSize(true);     

		cc.eventManager.addListener({
			//This will detect finger and the mouse touch
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: true, 
			onTouchBegan: this.onTouchBegan,
			onTouchMoved: this.onTouchMoved,
			onTouchEnded: this.onTouchEnded,
		}, this);
    },


	onTouchBegan : function(touch, event)
	{
		var tp = touch.getLocation();
		console.Log('Touched' + tp.x.toFixed(2) + ',' + tp.y.toFixed(2));
		return true;
	},

	onTouchMoved : function(touch, event)
	{
		var tp = touch.getLocation();
		console.Log('Touched' + tp.x.toFixed(2) + ',' + tp.y.toFixed(2));
	},
	onTouchEnded : function(touch, event)
	{
		var tp = touch.getLocation();
		console.Log('Touched' + tp.x.toFixed(2) + ',' + tp.y.toFixed(2));
	},



    // called every frame
    update: function (dt) 
	{
		
    },
});
 